require 'open-uri'
require 'json'

# Get address from user and formats it to be usable with google API
puts "Enter an address:"
address = gets
address = address.chomp.gsub(" ", "+")

# Retreives the longitude and latitude coordinates of address input by user
json_data = open("https://maps.googleapis.com/maps/api/geocode/json?address=#{address}&sensor=false").read
data = JSON.parse(json_data)

lat = data["results"][0]["geometry"]["location"]["lat"]
lng = data["results"][0]["geometry"]["location"]["lng"]

# Retrieves weather info from openweathermap API by using coordiantes of address input by  user
json_data = open("http://api.openweathermap.org/data/2.5/weather?lat=#{lat}&lon=#{lng}").read
data = JSON.parse(json_data)
kTemp = data["main"]["temp"]
fTemp = (((kTemp - 273.15) * 1.8) + 32).round(1) # Converts kelvin to fahrenheit and rounds to one decimal place
city = data["name"]

# Print results to console
puts "The temperature in #{city} is #{fTemp} degrees Fahrenheit"


