require 'open-uri'
require 'json'

# Get addresses from user and formats them to be usable with google API
puts "Enter a starting address:"
origin = gets
originURLFormat = origin.chomp.gsub(" ", "+")

puts "Enter a destination address:"
destination = gets
destinationURLFormat = destination.chomp.gsub(" ", "+")

# Request directions data from google
json_data = open("https://maps.googleapis.com/maps/api/directions/json?origin=#{originURLFormat}&destination=#{destinationURLFormat}&sensor=false").read
data = JSON.parse(json_data)
duration = data["routes"][0]["legs"][0]["duration"]["text"]

# Print results to console
puts "It takes #{duration} to drive from #{origin.chomp} to #{destination.chomp}."
