require 'open-uri'
require 'json'

# Get address from user and formats it to be usable with google API
puts "Enter an address:"
address = gets
address = address.chomp.gsub(" ", "+")

# Request coordinate data from google
json_data = open("https://maps.googleapis.com/maps/api/geocode/json?address=#{address}&sensor=false").read
data = JSON.parse(json_data)
lat = data["results"][0]["geometry"]["location"]["lat"]
lng = data["results"][0]["geometry"]["location"]["lng"]

# Prints longitude and lattitude to console
puts "Lattitude: #{lat} \nLongitude: #{lng}"
